﻿using System;
using DryIoc;
using UnitTestActivity.Services;
using Xunit;

namespace UnitTestActivity.Test.Services
{
    public class ProductServiceTest
    {
        private IContainer GetIoCContainer()
        {
            // *** For Activity 2.2 & 2.3
            // create an IoCContainer
            // register your dependencies
            return null;
        }

        private ProductService GetProductService()
        {
            // create your product service here
            return null;
        }

        [Fact]
        public void ProductServiceReturnsAllProducts()
        {
            // Arrange
            // get a product service from the method above

            // Act
            // do something with the product service - what are we testing?

            // Assert
            // validate that the results are what was expected

            throw new Exception("Not Done Yet!");
        }
    }
}