﻿using Xunit;

namespace UnitTestActivity.Services
{
    public class CreditCardServiceTests
    {
        [Fact]
        public void IsCreditCardValid_NotQuiteRight()
        {
            // arrange
            var cardNumber = "1234123412341234";
            var cardHolderName = "Joe";
            var expirationMonth = 5;
            var expirationYear = 2020;
            var ccvNumber = "333";

            var creditCardService = new CreditCardService();

            // act
            var result = creditCardService.IsCreditCardValid(cardNumber, cardHolderName, expirationMonth, expirationYear, ccvNumber);

            // assert
            Assert.Equal(true, result);
        }
    }
}
