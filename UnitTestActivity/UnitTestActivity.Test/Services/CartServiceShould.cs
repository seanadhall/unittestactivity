﻿using FluentAssertions;
using UnitTestActivity.Services;
using Xunit;

namespace UnitTestActivity.Test.Services
{
    public class CartServiceShould
    {
        [Fact]
        public void TestMethod1()
        {
            //Assemble
            var cartService = new CartService();

            //Act
            var result = cartService.GetDummyCartNumber();

            //Assert
            result.ShouldBeEquivalentTo(5);
        }
    }
}
