﻿namespace UnitTestActivity.Entities
{
    public class Item : Product
    {
        public Item() { }

        public Item(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            Quantity = 1;
        }

        public int Quantity { get; set; }
    }
}