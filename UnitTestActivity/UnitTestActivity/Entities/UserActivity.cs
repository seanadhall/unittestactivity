﻿using System;

namespace UnitTestActivity.Entities
{
    public class UserActivity
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime ActivityDate { get; set; }
    }
}