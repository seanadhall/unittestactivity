﻿using System.Collections.Generic;

namespace UnitTestActivity.Entities
{
    public class Cart
    {
        public Cart()
        {
            Items = new List<Item>();
        }

        public int UserId { get; set; }
        public List<Item> Items { get; set; }
    }
}