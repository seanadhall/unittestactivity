﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using UnitTestActivity.ExternalConcerns;

namespace UnitTestActivity.Services
{
    public class ReportingService
    {
        public List<string> GetReportNames()
        {
            var reportPath = GetReportPath();
            var folder = new DirectoryInfo(reportPath);
            var files = folder.GetFiles("*.*", SearchOption.TopDirectoryOnly);
            return files.Select(f => f.Name).ToList();
        }

        public byte[] GetReport(string reportName)
        {
            var reportPath = GetReportPath();
            var fullPath = Path.Combine(reportPath, reportName);
            if (!File.Exists(fullPath))
            {
                throw new FileNotFoundException($"Cannot find {reportName}");
            }

            var reportContents = File.ReadAllText(fullPath);
            var bytes = Encoding.ASCII.GetBytes(reportContents);
            return bytes;
        }

        public void GenerateReport()
        {
            var reportPath = GetReportPath();

            var reportLines = new StringBuilder();

            var db = new InMemoryDb();
            var users = db.GetAllUsers();
            foreach (var user in users)
            {
                reportLines.AppendLine($"User: {user.Username} / {user.Id}");

                var activities = db.GetUserActivities(user.Id);

                foreach (var activity in activities)
                {
                    reportLines.AppendLine($"{activity.ActivityDate.ToString("G")} - {activity.Description}");
                }
            }

            var filename = $"Report{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff")}";
            var fullPath = Path.Combine(reportPath, filename);

            File.WriteAllText(reportLines.ToString(), fullPath);
        }

        private string GetReportPath()
        {
            var reportPath = ConfigurationManager.AppSettings["ReportPath"];
            if (!Directory.Exists(reportPath))
            {
                Directory.CreateDirectory(reportPath);
            }
            return reportPath;
        }
    }
}