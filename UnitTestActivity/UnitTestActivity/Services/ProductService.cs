﻿using System;
using UnitTestActivity.ExternalConcerns;
using UnitTestActivity.Entities;
using System.Collections.Generic;
using System.Text;

namespace UnitTestActivity.Services
{
    public class ProductService
    {
        private InMemoryDb Db;
        public ProductService()
        {
            Db = new InMemoryDb();
        }

        public void DisplayProducts()
        {
            var list = GetProductsList();
            var output = ConvertListToOutputString(list);
            WriteList(output);
        }

        public List<Product> GetProductsList()
        {
            return Db.GetAllList();
        }

        protected string ConvertListToOutputString(List<Product> list)
        {
            var sb = new StringBuilder();
            foreach (Product product in list)
            {
                sb.Append(product.Name);
                sb.Append(" | $");
                sb.Append(product.Price);
                sb.Append(" | ");
                sb.Append(product.Description);
                sb.Append(Environment.NewLine);
            }

            return sb.ToString();
        }

        protected void WriteList(string output)
        {
            Console.Write(output);
        }
    }
}