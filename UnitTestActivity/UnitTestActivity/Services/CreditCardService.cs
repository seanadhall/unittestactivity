﻿using System;
using System.Globalization;
using UnitTestActivity.ExternalConcerns;

namespace UnitTestActivity.Services
{
    public class CreditCardService
    {
        private Logger logger = new Logger();

        public bool ChargeCreditCard(string cardNumber, string cardHolderName, int expirationMonth, int expirationYear, string ccvNumber, decimal amount)
        {
            var ccService = new CreditCardWebService();
            var retryCounter = 0;
            while (retryCounter < 3)
            {
                try
                {
                    var result = ccService.ApplyCredit(cardNumber, cardHolderName, expirationMonth, expirationYear, ccvNumber, amount);
                    logger.Info($"Credit card charge applied (retry count: {retryCounter})");
                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error("Exception while charging credit card", ex);
                    System.Threading.Thread.Sleep(10000);
                    retryCounter++;
                }
            }

            throw new Exception("Unable to communicate with credit card service.");
        }

        public bool IsCreditCardValid(string cardNumber, string cardHolderName, int expirationMonth, int expirationYear, string ccvNumber)
        {
            var ccService = new CreditCardWebService();
            var result = ccService.IsValid(cardNumber, cardHolderName, expirationMonth, expirationYear, ccvNumber);
            return result;
        }

        public bool IsCreditCardExpired(int expirationMonth, int expirationYear)
        {
            try
            {
                var currentDate = DateTime.Now;
                var ccExpirationDate = DateTime.ParseExact($"{expirationMonth:D2}/01/{expirationYear:D4}", "MM/dd/yyyy", CultureInfo.InvariantCulture).AddMonths(1);
                var isExpired = currentDate >= ccExpirationDate;
                return isExpired;
            }
            catch (Exception ex)
            {
                var errorMessage = "Expiration date values are not valid";
                logger.Error(errorMessage, ex);
                throw new Exception(errorMessage);
            }
        }
    }
}