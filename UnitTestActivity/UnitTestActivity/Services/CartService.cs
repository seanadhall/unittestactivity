﻿using System;
using System.Linq;
using UnitTestActivity.Entities;

namespace UnitTestActivity.Services
{
    public class CartService
    {
        private const string TxStateCode = "TX";
        private const double TxSalesTaxMultiplier = .0825;
        private const double StandardShippingCost = 5.99;

        public int GetDummyCartNumber()
        {
            return 5;
        }
		
        public void AddItemToCart(ref Cart cart, Product item)
        {
            cart.Items.Add(new Item(item));
        }

        public void RemoveItemFromCart(ref Cart cart, int itemId)
        {
            cart.Items.RemoveAll(a => a.Id == itemId);
        }

        public void UpdateItemQuantity(ref Cart cart, int itemId, int quantity)
        {
            cart.Items.First(a => a.Id == itemId).Quantity = quantity;
        }

        public double GetCartSubtotal(Cart cart)
        {
            return cart.Items.Select(a => a.Price * a.Quantity).Sum();
        }

        public double GetDiscountByPercentOff(Cart cart, int percentOff)
        {
            var discountMultiplier = Math.Abs(percentOff) / 100.00;
            return GetCartSubtotal(cart) * discountMultiplier;
        }

        public double GetSalesTaxByState(string stateCode)
        {
            if (string.Equals(stateCode, TxStateCode, StringComparison.CurrentCultureIgnoreCase))
            {
                return TxSalesTaxMultiplier;
            }

            throw new Exception("State was not found.");
        }

        public double GetCartTotal(Cart cart, Address customerAddress, int percentOff = 0)
        {
            double total = 0;

            total += GetCartSubtotal(cart);
            total -= GetDiscountByPercentOff(cart, percentOff);
            total += total * GetSalesTaxByState(customerAddress.State);
            total += total > 0 ? StandardShippingCost : 0;

            return Math.Round(total,2);
        }
    }
}
