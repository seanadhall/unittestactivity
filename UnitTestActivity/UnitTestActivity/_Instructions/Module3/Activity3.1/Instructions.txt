###############################################################################
###  Activity 3.1 - Use code coverage to analyze/fix a test (15 min)        ###
###############################################################################

1. Using Resharper, cover all tests in the solution.  

2. Examine the hierarchy structure in the coverage tree.  Answer the questions in 
your head.
- Do the coverage numbers make sense to you?
- Which services / functions stand out as needing additional testing?

3. Double-click CreditCardService.IsCreditCardValid.  This should open the 
class/routine in Visual Studio.  Answer the questions in your head.
- Do you see any visual indicators suggesting whether specific lines are covered?  
- What color are the indicators?
- What forms of test coverage are handled by Resharper? 

4. Return to the coverage tree, right-click on the IsCreditCardValid function, 
and select "Show Covering Tests".  Locate the resulting modal window and 
double-click on the test to open it in Visual Studio.

5. As the name indicates, this test is not quite right.  Analyze the test by 
setting a debug breakpoint at the start and debugging the test (right-click in 
the test function's body and select "Debug Unit Tests").

- Is this your first time to debug a unit test?  Neat huh?
- Be sure to "step into" constructors/functions (use F11 to step into a function).
- Did you encounter any code that would behave inconsistently over time and/or in 
  different environments?  What do we call this type of code?

6. Refactor the application code and/or tests such that the test passes 
consistently without changing the run-time behavior of the application code.

- You have identified an external concern in step 5.  You want to be able to mock 
  this external concern when running IsCreditCardValid from a test.

7. Once you have a solution, run the test again to be sure it passes consistently.

8. Once the test passes consistently, use Resharper to cover all tests in the 
solution again.  

- Did the numbers improve?  
- Is the IsCreditCardValid function fully covered?  What other scenarios should be tested?

-- break for discussion

