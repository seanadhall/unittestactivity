﻿namespace UnitTestActivity.ExternalConcerns
{
    public class CreditCardWebService
    {
        public bool IsValid(string cardNumber, string cardHolder, int expirationMonth, int expirationYear, string ccvNumber)
        {
            return ExternalConcern.ExternalBool(0);
        }

        public bool ApplyCredit(string cardNumber, string cardHolder, int expirationMonth, int expirationYear, string ccvNumber, decimal amount)
        {
            return ExternalConcern.ExternalBool();
        }
    }
}