﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestActivity.ExternalConcerns
{
    public static class ExternalConcern
    {
        public static void ExternalVoid(int max=2)
        {
            // The results will vary and the service may or may not be available.
            var random = new Random(DateTime.Now.Millisecond);
            if (random.Next(0, max) > 0)
                return;
            throw new Exception("Endpoint not available");
        }

        public static bool ExternalBool(int max=2)
        {
            // The results will vary and the service may or may not be available.
            var random = new Random(DateTime.Now.Millisecond);
            if (random.Next(0, max) > 0)
                return true;
            throw new Exception("Endpoint not available");
        }

        public static List<int> ExternalNumberList(int max = 2)
        {
            // The results will vary and the service may or may not be available.
            var random = new Random(DateTime.Now.Millisecond);
            if (random.Next(0, max) == 0)
                throw new Exception("Endpoint not available");
            return Enumerable.Range(0, random.Next(0, max)).ToList();
        }
    }
}