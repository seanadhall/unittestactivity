﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestActivity.Entities;

namespace UnitTestActivity.ExternalConcerns
{
    public class InMemoryDb
    {
        private List<Product> products = new List<Product>()
        {
            new Product() {Name = "Pilsner", Description = "Crisp, classic German Pilsner", Price = 3},
            new Product() {Name = "Stout", Description = "Full-bodied English Stout", Price = 5},
            new Product() {Name = "Blonde", Description = "Light Bodied Belgian-Style Ale", Price = 2},
            new Product() {Name = "Dubbel", Description = "Dark Belgian Style Ale with notes of dark stone fruit", Price = 6},
            new Product() {Name = "American Amber", Description = "Sweet, Malty and rich deep flavor with a spicy finish", Price = 4},
        };

        public List<Product> GetAllList()
        {
            return products;
        }

        public List<User> GetAllUsers()
        {
            return ExternalConcern
                .ExternalNumberList(4)
                .Select(n => new User() {Id = n, Username = $"User{n:D8}"})
                .ToList();
        }

        public List<UserActivity> GetUserActivities(int userId)
        {
            return ExternalConcern
                .ExternalNumberList(4)
                .Select(n => new UserActivity() { Id = n, Description = $"UserActivity{n:D8}", ActivityDate = DateTime.Now.AddDays(0-n) })
                .ToList();
        }
    }
}