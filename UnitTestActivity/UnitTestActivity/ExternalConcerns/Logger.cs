﻿using System;

namespace UnitTestActivity.ExternalConcerns
{
    public class Logger
    {
        public void Debug(string message)
        {
            ExternalConcern.ExternalVoid();
        }

        public void Info(string message)
        {
            ExternalConcern.ExternalVoid();
        }

        public void Warn(string message)
        {
            ExternalConcern.ExternalVoid();
        }

        public void Warn(string message, Exception ex)
        {
            ExternalConcern.ExternalVoid();
        }

        public void Error(string message)
        {
            ExternalConcern.ExternalVoid();
        }

        public void Error(string message, Exception ex)
        {
            ExternalConcern.ExternalVoid();
        }
    }
}